
package figuras;
public class Figuras {

    public static void main(String[] args)
    {
    System.out.println("-----Figuras Geometricas------\n");

        //Triangulo
    float r;
    Triangulo figura1=new Triangulo();
    r=figura1.calcularPerimetro();
    System.out.println("El perimetro del Triangulo es de: "+r);

    //cuadrado
    float lado=4;
    Cuadrado cuadrado = new Cuadrado();
    String nombre=cuadrado.ObtenerNombre();
    lado=cuadrado.calcularPerimetro(lado);
    System.out.println("La figura "+nombre+" Tiene como perimetro "+lado);
    
    //circulo
    Circulo objFigura = new Circulo();
    String s=objFigura.obtenerNombre();
    float a=objFigura.calcularPerimetro();
    System.out.println("La figura "+s+" tiene como perimetro "+a);
    
    }
}
