package figuras;

/**
 *
 * @author al346802
 */
public class Circulo extends Figura{
    
    public float calcularPerimetro(){
        float r=1;
        float perimetro = (float) (2*(3.1416)*r);
        return perimetro;
        
    }
    protected String obtenerNombre(){ 
        return "Circulo";
    }
}
