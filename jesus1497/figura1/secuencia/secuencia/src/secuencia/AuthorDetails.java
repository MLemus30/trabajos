/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package secuencia;
import java.*;
/**
 *
 * @author al346802
 */
public class AuthorDetails 
{
    private String name;
    private String direction;
    private String email;
    private String cel;

    public String getName() {
        return name;
    }

    public String getDirection() {
        return direction;
    }

    public String getEmail() {
        return email;
    }

    public String getCel() {
        return cel;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCel(String cel) {
        this.cel = cel;
    }

    
    
}
