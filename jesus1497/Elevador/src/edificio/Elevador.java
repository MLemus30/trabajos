/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edificio;

/**
 *
 * @author Jesus14
 */
public class Elevador 
{
    boolean sube;
    boolean baja;
    boolean primerPiso;
    boolean oscioso;
    boolean moviendosePrimerPiso;
    
    public int pisoActual = 1;
    public int contieneElevadorPersona = 1;
    public int ticks = 0;
    
    public void sube(){
        primerPiso = false;
        sube = true;
        baja = false;
        oscioso = false;
        
        pisoActual++;
        printEstado();
        ticks++;
    }
    public void objetivo()
    {
        baja = false;
        sube = false;
        oscioso = true;
        if(moviendosePrimerPiso){
            primerPiso = true;
        }
        else
        {
            moviendosePrimerPiso = false;
            printEstado();
            ticks++;
        }
    }
    public void baja(){
        baja = true;
        sube = false;
        oscioso = false;
        if(pisoActual == 2)
        {
            moviendosePrimerPiso = true;
        }
        if(moviendosePrimerPiso){
            primerPiso = true;
            pisoActual--;
            moviendosePrimerPiso = false;
        }
        else{
            pisoActual--;
            printEstado();
            ticks++;
        }
        
    }
    public void terminoElTiempo(){
        if(!primerPiso && ticks >= 10){
            System.out.println(ticks +", Termino el tiempo!");
        }
        while(!primerPiso && ticks >= 10){
            baja();
        }
        objetivo();
    }
    public void printEstado(){
        if(sube)
        {
            System.out.println("Sube!, al piso" + pisoActual);
        }
        if(baja){
            System.out.println("Baja!, al piso" + pisoActual);
        }
        if(oscioso){
            System.out.println("Ya llegue al piso" + pisoActual);
        }
    
    }
    
}
