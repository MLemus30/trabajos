/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ComReloj;
class Reloj{
//agregacion de bateria Fuera del constructor
private Bateria bat;
private Caratula car;

public Reloj(){
//Composicion Manecillas
Manecillas mane = new Manecillas();
//Composicion Caratula
Caratula cara = new Caratula();
//Composicion Maquinaria
Maquinaria maquina = new Maquinaria();
}

public void setEstadoBateria(Bateria porcentaje){
    bat = porcentaje;
}

public int getBateria(){
    return bat.porcentaje;
}
  
}
class Bateria{
   public int porcentaje=50;
}

class Manecillas{
public void hora(float hor){
    System.out.println("Son las "+hor);
}
}

class Caratula{
    
}

class Maquinaria{

}
public class ComReloj{
    public static void main(String[] args) {
      Reloj rel=new Reloj();
      Bateria bat=new Bateria();
      rel.setEstadoBateria(bat);
      System.out.println(rel.getBateria());
      System.out.println(bat.porcentaje);
      rel=null;
      System.out.println(bat.porcentaje);
    }
    
}
