
package codificardiagramade;
class Administrador extends GestorDeContenidos{
   GestorDeContenidos contenidos = new GestorDeContenidos();  
   AutorDetails autor = new AutorDetails();
//Se comunica con GestorDeContenidos
  public String createNewBlogAcount(){
      return "Nuevo Blog Creado\n"; //agregar algo en el return
  } 
  //Se comunica con GestorDeContenidos
  public String selectBlogAccountType(String blog){
      return "Se selecciono el blog "+blog;
  }
  //Se comunica con GestorDeContenidos
  //Desencadena una serie de mensajes
  public void enterAuthorDetails(String nombre,int id){ 
      autor.nombre=nombre;
      autor.ID=id;
      contenidos.checkAutorDetails(autor);
      contenidos.createNewRegularBlogAccount(autor);
      contenidos.emailBlogDetails();
  }
}
