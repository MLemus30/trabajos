package comunicacion;

import comunicacion.Personas.Persona;
import java.util.Scanner;


public class Mensaje implements MsgCompatible{
    private String texto;
    Scanner sc = new Scanner (System.in);
    public void setMsg(String texto){
      System.out.println("El mensaje es: "+texto+" Listo para enviar");
    }
    public String getMsg(){
        System.out.println("Cual es tu mensaje?");
        texto=sc.nextLine();
        setMsg(texto);
        return texto;
    }
    public boolean compatible(Persona P1,Persona P2){
        return false;  
    }
}
