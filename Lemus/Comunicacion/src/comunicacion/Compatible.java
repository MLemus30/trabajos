package comunicacion;
import comunicacion.Personas.Persona;

public interface Compatible {
  public boolean compatible(Persona p1,Persona p2,Mensaje msg);
}
