/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edificios;
    public class Elevador {
     private boolean subiendo;
     private boolean oscioso;
     private boolean bajando;
     private int piso;
     
     public void Elevador(){
         piso=piso+1; 
         System.out.println("Estoy en el piso"+piso+"\n");
     }
     
     
     public void sube() throws InterruptedException{
       subiendo=true;
       bajando=false;
       oscioso=false;
       piso=piso+1;
       System.out.println("Subiendo a piso "+piso+"\n");
       Thread.sleep(2000);
     }
     
     public void llego() throws InterruptedException{
       oscioso=true;
       subiendo=false;
       bajando=false;
       System.out.println("Llego a piso "+piso+"\n");
       Thread.sleep(2000);
     }
     
       public void MoverPrimerPiso() throws InterruptedException{
          System.out.println("Moviendome a piso 1");
          piso=1;
          Thread.sleep(2000);
          llego();
     }
     
     public void baja() throws InterruptedException{
         bajando=true;
         oscioso=false;
         subiendo=false;
         piso=piso-1;
       System.out.println("Bajando a piso "+piso+"\n");
       Thread.sleep(2000);
     }
     
     public void oscioso() throws InterruptedException{
       System.out.println("Oscioso\n");
       Thread.sleep(10000);
       TiempoTerminado();
     }
     
     public String TiempoTerminado() throws InterruptedException{ 
         MoverPrimerPiso();
         return "Tiempo Terminado\n";
     }
}
