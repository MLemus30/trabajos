﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaElevador
{
    class Program
    {
        static void Main(string[] args)
        {
            Elevador Elevador1 = new Elevador();
            Elevador1.llego();
            Elevador1.sube();
            Elevador1.sube();
            Elevador1.llego();
            Console.WriteLine("Bajo 1 persona y subio otra");
            Elevador1.baja();
            Elevador1.llego();
            Console.WriteLine("Bajo 1 persona y subio otra");
            Elevador1.sube();
            Elevador1.sube();
            Elevador1.sube();
            Elevador1.llego();
            Console.WriteLine("Bajo 1 persona");
            Elevador1.terminoElTiempo();
            Console.ReadKey(true);
        }
    }
}
