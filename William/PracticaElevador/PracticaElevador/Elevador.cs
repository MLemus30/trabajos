﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracticaElevador
{
    class Elevador
    {
        Boolean enPrimerPiso;
        Boolean subiendo;
        Boolean bajando;
        Boolean oscioso;
        Boolean moviendoAlPrimerPiso;
        public int pisoActual = 1;
        public int numeroDePersonas = 1;
        public int ticks = 0;

        public void sube()
        {
            enPrimerPiso = false;
            subiendo = true;
            bajando = false;
            oscioso = false;

            pisoActual++;
            printEstado();
            ticks++;
        }
        public void llego()
        {
            bajando = false;
            subiendo = false;
            oscioso = true;
            if (moviendoAlPrimerPiso)
            {
                enPrimerPiso = true;
            }
            moviendoAlPrimerPiso = false;
            printEstado();
            ticks++;
        }
        public void baja()
        {
            bajando = true;
            subiendo = false;
            oscioso = false;
            if(pisoActual==2)
            {
                moviendoAlPrimerPiso = true;
            }
            if (moviendoAlPrimerPiso)
            {
                enPrimerPiso = true;
                pisoActual--;
                moviendoAlPrimerPiso = false;
            }
            else
                pisoActual--;
            printEstado();
            ticks++;
        }
        public void terminoElTiempo()
        {
            if(!enPrimerPiso && ticks >= 10)
                Console.WriteLine(ticks+"| Termino el tiempo!!");
            while (!enPrimerPiso && ticks>=10)
            {
                baja();
            }
            llego();
        }
        public void printEstado()
        {
            if(subiendo)
            Console.WriteLine(ticks+"| Subiendo! al piso "+pisoActual);
            if(bajando)
            Console.WriteLine(ticks+"| Bajando! al piso "+pisoActual);
            if(oscioso)
            Console.WriteLine(ticks+"| Ya llegue al piso "+pisoActual);
        }
    }
}
